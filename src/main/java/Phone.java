 abstract public class Phone {

     private String name;
     private String model;
     private int camera;
     private int battery;

     public Phone(String name, String model, int camera, int battery) {
         this.name = name;
         this.model = model;
         this.camera = camera;
         this.battery = battery;
     }



     public String getName() {
         return name;
     }

     public void setName(String name) {
         this.name = name;
     }

     public String getModel() {
         return model;
     }

     public void setModel(String model) {
         this.model = model;
     }

     public int getCamera() {
         return camera;
     }

     public void setCamera(int camera) {
         this.camera = camera;
     }

     public int getBattery() {
         return battery;
     }

     public void setBattery(int battery) {
         this.battery = battery;
     }
 }

